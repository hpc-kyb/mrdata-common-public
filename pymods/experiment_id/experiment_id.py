
import yaml
import uuid
import os
import time
import hashlib
import base64

trc = False

def int_to_bytes(i: int, *, signed: bool = False) -> bytes:
    length = ((i + ((i * signed) < 0)).bit_length() + 7 + signed) // 8
    return i.to_bytes(length, byteorder='big', signed=signed)

def bytes_to_int(b: bytes, *, signed: bool = False) -> int:
    return int.from_bytes(b, byteorder='big', signed=signed)

# Create a error detecting expiriment name based on the experiment number
# A rough prototype in bash follows.
# ( EXPID=100002; echo $EXPID > /dev/tty; echo -n $EXPID | md5sum | cut -b 1-4 ; echo -n $EXPID ) | xxd -r -p | base32 |      tee /dev/tty     | base32 -d | xxd -p | cut -b 5-10


def experiment_number_to_id( experiment_number ):
    int_exp_id = int( experiment_number )
    assert( int_exp_id >= 100000 and int_exp_id <= 999999 )

    str_exp_id = str( int_exp_id )

    if trc: print( "str_exp_id: " + str_exp_id )

    experiment_id_hash = hashlib.md5( int_to_bytes( int_exp_id ) )
    exp_hash_int = int( experiment_id_hash.hexdigest(), 16 )
    exp_hash_str = str( exp_hash_int )
    exp_hash_substr = exp_hash_str[0:6]
    if trc: print( "exp_id_hash: " + exp_hash_substr )

    hash_plus_exp = int( exp_hash_substr ) * 1000000 + int_exp_id
    if trc: print( "hash_plus_exp: ", str(hash_plus_exp) )

    hash_plus_exp_b32 = base64.b32encode( int_to_bytes( hash_plus_exp ) )

    exp_hash_id = hash_plus_exp_b32.decode('utf-8')
    if trc: print( "hash_plus_exp base32 encode: ", exp_hash_id )

    exp_hash_id = exp_hash_id[:4] + "-" + exp_hash_id[4:]
    if trc: print( "experiment_id: ", exp_hash_id )

    return exp_hash_id


# Validate

def experiment_id_validate( experiment_id ):
    try:
        if len( experiment_id ) != 9 :
            if trc: print( "experiment_id not 9 chars. experiment_id: ", experiment_id )
            return False

        if experiment_id[4:5] != '-' :
            if trc: print( "experiment_id 5th char not - (hyphen). experiment_id:", experiment_id )
            return False

        exp_hash_id = experiment_id[:4] + experiment_id[5:]
        if trc: print( "exp_hash_id: ", exp_hash_id )

        hash_plus_exp_b32_rev = exp_hash_id.encode('utf-8')
        if trc: print( "hash_plus_exp_b32_rev: ", hash_plus_exp_b32_rev )

        hash_plus_exp_rev = bytes_to_int( base64.b32decode( hash_plus_exp_b32_rev ) )
        if trc: print( "hash_plus_exp_rev: ", hash_plus_exp_rev )

        int_exp_id_rev = hash_plus_exp_rev % 1000000
        if trc: print( "int_exp_id_rev: ", int_exp_id_rev )

        if int_exp_id_rev < 100000 or int_exp_id_rev > 999999 :
            if trc: print( "out of range" )
            return False

        exp_hash_id_rev = experiment_number_to_id( int_exp_id_rev )

        rc = exp_hash_id_rev == experiment_id
        return rc
    except Exception as ex:
        if trc: print(" Failed to validate id: " + str(a_experiment_id) + " exr: " + str( ex ) )
    return False

if __name__ == "__main__":
    #id = experiment_number_to_id( "999997" )
    #print( "* Generated Experiment id: ", id )
    #
    #print( "* Validate Experiment Id: ", experiment_id_validate( id ) )
    #
    ## Maximum
    #print( "Maximum constant 999999999999 base32 encode: ", base64.b32encode( int_to_bytes( 999999000000 + 999999 ) ) )


    for i in range(20):
        num = 100000 + i
        print( " i: ", str( num ), " experiment id: ", experiment_number_to_id( num ) )

    num = 123456
    trc = False
    while True:
        id = experiment_number_to_id( num )
        valid = experiment_id_validate( id )
        if not valid or num == 123283 or id == "IC5G-LCTG":
            trc = True
            print( "FAIL at num: ", num, " id: ", id )
            id = experiment_number_to_id( num )
            valid = experiment_id_validate( id )
            trc = False
        num += 1




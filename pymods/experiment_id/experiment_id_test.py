import sys
import experiment_id as xid

print("testing experiment id")


#for i in range(20):
#    num = 100000 + i
#    print( " i: ", str( num ), " experiment id: ", xid.experiment_number_to_id( num ) )

id_for_100000 = "DWSW-PYHA"
if xid.experiment_number_to_id( 100000 ) != id_for_100000:
    print("First possible  experiment number (100000) fails to generate correct id: " + id_for_100000)
    sys.exit(-1)
if not xid.experiment_id_validate( id_for_100000 ):
    print("First possible experiment number (100000) fails to validate as id: " + id_for_100000)
    sys.exit(-1)

valid_dict = {}

#Put a test duplicate in.
test_id = "JHQO-VWJR"
test_num = 100017
valid_dict[ test_id ] = test_num

for i in range(100000,999999):
    num = i
    id = xid.experiment_number_to_id( num )
    valid = xid.experiment_id_validate( id )
    if not valid:
        print("ID Failed to validate  id:" + str(id) + "valid:" + str(valid))
        continue
    if id in valid_dict:
        if id != test_id or num != test_num:
            print("ID duplicate found. id:" + str(id) + " num " + str(num) + " prev num: " + str(valid_dict[id]) ) 
    valid_dict[ id ] = num

print("Finished validating all usable experiment ids from 100000 to 999999")

"""
num = 123456
trc = False
while True:
    id = xid.experiment_number_to_id( num )
    valid = xid.experiment_id_validate( id )
    if not valid or num == 123283 or id == "IC5G-LCTG":
        trc = True
        print( "FAIL at num: ", num, " id: ", id )
        id = xid.experiment_number_to_id( num )
        valid = xid.experiment_id_validate( id )
        trc = False
    num += 1
"""
# python configuration definitions for MrData iRODS structure

# NEED TO GET RID OF THIS STYLE OF CONF


# mount point of the landing zone
#lz_basepath = "/RDS_Samba"

import os

# Check env for basepath. This enables testing.
lz_basepath = os.environ.get("MRDENV_MRI_ETL_PATH_DICOM_LZ")

# ... if not found, set.
if lz_basepath == None:
    lz_basepath = "/RDS_Samba"

assert isinstance( lz_basepath, str ) #mypy

# uploader log file path
uploader_log_path = lz_basepath + "/LOGS"

# twix uploader landing path
twix_lz_path = lz_basepath + "/RDS/94Tuebingen/allData"

# dicom uploader landing path
dicom_lz_path = lz_basepath

# extended experiment data uploader landing path
exp_data_lz_path = lz_basepath + "/EXPERIMENT_DATA"

processed_lz_path = lz_basepath + "/PROCESSED"

# echtdata experiment creation lockfile

experiment_creation_lockfile = "/home/mradmin/uploader-experiment-creation-lockfile.txt"

# This info is duplicated in the credentials. TODO: find a way to have a unified source.
irods_zone = "MRDataZone"
mrdata_base_ipath = "/" + irods_zone + "/home/" + "mrdata"
cast_base_ipath   = "/" + irods_zone + "/home/" + "castellum"
mrdataforms_base_ipath   = "/" + irods_zone + "/home/" + "mrdataforms"

# Echtdata path
# /MrdataZone/home/mrdata/echtdata/studies/<studyId>/experiments/<experimentId>/<data-source>/...

# Metadata path
# /MrdataZone/home/mrdata/metadata/experiments/E/X/<EXperimentId>/<data-source>/...
mrdata_metadata_ipath = mrdataforms_base_ipath + "/metadata"

# This structure of ipath avoids directories with tens of thousands of files/subdirs.
# The experiment id is base32 encoded, so with 1M experiments, the lowest dir would have 1k subdirs
def get_metadata_experiment_ipath( experiment_id ):
    ipath = mrdata_metadata_ipath + "/experiments/" + experiment_id[0] + "/" + experiment_id[1] + "/" + experiment_id
    return ipath

import os
import sys
import errno
import time
import datetime
import glob
import uuid
import yaml
import re
import logging
import hashlib
import base64
import ssl

from pathlib import Path
from filelock import Timeout, FileLock

from irods.session import iRODSSession
from irods.meta import iRODSMeta, AVUOperation
import irods.keywords as kw
import irods.test.helpers as helpers
import irods.exception as irods_ex

def get_irods_session_from_cred_yaml( cred_file_name,
                                      cafile='/irods_ssl_ca_certificate_file',
                                      time_out_secs=None ):

    logger = logging.getLogger(__name__)
    try:
        with open( cred_file_name, "r" ) as cf:
            cred_file_list = yaml.load( cf, Loader=yaml.FullLoader )
    except IOError as e:
        logger.error("Failed to open credential file " + cred_file_name + " " + str(e) )
        return None, None

    # NEED to do this the right way!
    irods_host = cred_file_list["host"]

    env_irods_host = os.getenv( "MRDENV_IRODS_HOSTNAME" )
    if env_irods_host != None:
        logger.debug( "IRODS HOSTNAME set in env. hostname: " + str( env_irods_host ))
        irods_host = env_irods_host   # docker-compose.yml service name for irods -- pull this up to config

    host=irods_host
    port=cred_file_list["port"]
    zone=cred_file_list["zone"]
    user=cred_file_list["user"]
    password=cred_file_list["password"]

    return get_irods_session( host, port, zone, user, password,
                              cafile=cafile,
                              time_out_secs=time_out_secs)


def get_irods_session( host, port, zone, user, password,
                       cafile='/irods_ssl_ca_certificate_file',
                       do_not_block=False, time_out_secs=None ):
    logger = logging.getLogger(__name__)

    backoff = 0
    time_waited = 0

    while True:
        irods_sesh = None
        try:
            ssl_context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH,
                                                     cafile=cafile,
                                                     capath=None,
                                                     cadata=None)

            ssl_settings = {'client_server_negotiation': 'request_server_negotiation',
                           'client_server_policy': 'CS_NEG_REQUIRE',
                           'encryption_algorithm': 'AES-256-CBC',
                           'encryption_key_size': 32,
                           'encryption_num_hash_rounds': 16,
                           'encryption_salt_size': 8,
                           'ssl_context': ssl_context}

            irods_sesh = iRODSSession( host=host, port=port, zone=zone, user=user, password=password,
                                       **ssl_settings)

            #if irods_sesh != None :
            #    break
            # This will throw an exception if irods_sesh is not viable.
            # Trust but verify ... inside the try.
            if irods_sesh != None :
                # iRODS session time out disabled. We depend on docker or other outside health monitors
                irods_sesh.connection_timeout = 6000 # 100 minutes
                # This is a quick test to see that it is working. Could do this only in DEBUG mode.
                coll = irods_sesh.collections.get( "/" + str( irods_sesh.zone ) + "/home/" + str( irods_sesh.username ) )
                break; # this is the good exit of the while loop

        except Exception as e:
            # This seems fishy... but we are getting a bunch of zombie irods connects.
            logger.error("Exception opening irods session. ex: " + str(e) )
            logger.error( e )

            try:
                if irods_sesh != None :
                    irods_sesh.cleanup()
                    logger.error( "Cleaned up non-None irods_sesh after failing a 'get' ... strange." )
            except Exception as ex:
                logger.error( "Could not clean up non-None irods_sesh. ex: " + str (ex) )

            if do_not_block:
                logger.info( "Returning fail baesd on do_not_block==False.  do_not_block: " + str( do_not_block ) )
                return None, None

        if time_out_secs != None and time_waited >= time_out_secs:
          logger.error( "Timed out waiting for irods session connection. Seconds waited: " + str( int( time_waited ) ) )
          return None, None

        if backoff < 6:
            backoff += 1
        wait_time = 2**backoff
        if time_out_secs != None and wait_time + time_waited > time_out_secs:
            wait_time = time_out_secs - time_waited
        logger.info( "Waiting for iIRODS session. Seconds waited: " + str( int( time_waited ) ) )
        time.sleep( wait_time )
        time_waited += wait_time
        continue # Stay in wait

    logger.debug( "created irods session" )

    irods_user = str( irods_sesh.username )
    irods_zone = str( irods_sesh.zone )
    base_ipath = "/" + irods_zone + "/home/" + irods_user

    try:
        coll = irods_sesh.collections.get( base_ipath )
    except:
        logger.error("FATAL: Unable to preform collections.get() on castellum_base_ipath:" + base_ipath )
        return None, None

    logger.debug( "Test Succeeds: iRODS returns collection id for " + base_ipath + ": " + str(coll.id) )

    return irods_sesh, base_ipath

def get_irods_user(irods_session, irods_user_id):
    logger = logging.getLogger(__name__)
    irods_user = None
    error_msg = ""
    try:
        irods_user = irods_session.users.get( irods_user_id )
    except Exception as err:
        if isinstance( err, irods_ex.UserDoesNotExist):
            pass
        error_msg = "Failed to find irods_user_id with irods server. irods_user_id: " + irods_user_id
        logger.error( error_msg + " err: " + str(err))

    logger.info( "Got irods user. irods_user_id: " + irods_user_id + " irods_user: " + str( irods_user ) )
    return irods_user, error_msg



def AddAvu( coll, avu_attr, avu_value ):

    # In response to: not well-formed (invalid token):
    avu_attr = str(avu_attr)
    avu_value = str(avu_value)

    logging.debug( f"avu_attr: {avu_attr} avu_value: {avu_value}" )

    try:
        coll.metadata.add( avu_attr, avu_value )
    except Exception as ex:
        logging.error( "Failed to attach AVU attr: " + avu_attr +
                       " avu_value: " + avu_value +
                       " coll: " + coll.path + " ex: " + str(ex) )
        return False
    return True


def init():
    pass


# This scrubs off the AVU units field
def irods_avus_to_pydict( coll_or_obj ):

    # We expect metadata AVUs on the metadata experiment collection
    try:
        all_AVUs = coll_or_obj.metadata.items()
    except Exception as ex:
        logging.error("FAILED to access AVUs on iRODS collection." +
            " ipath: " + coll_or_obj.path + " ex: " + str(ex) )
        return None

    avu_dict = {}
    for avu in all_AVUs:
        logging.debug("coll_or_obj.path: " + coll_or_obj.path + " AVU: " + str(avu) )
        avu_dict[ avu.name ] = avu.value

    return avu_dict

nonprintable_regex_compiled = re.compile( r'[^\x20-\x7E]' )
def convert_non_ascii_to_underscore(s):
     return nonprintable_regex_compiled.sub( '_', s )

def filter_utf8_characters2(s):
    return ''.join(c if c.encode('utf-8', 'ignore').decode('utf-8', 'ignore') == c else '_' for c in s)

def filter_utf8_characters(s):
    valid_chars = []
    for c in s:
        try:
            c.encode('utf-8')
            valid_chars.append(c)
        except UnicodeEncodeError:
            valid_chars.append('_')
    return ''.join(valid_chars)

import unicodedata

def is_european_character(c):
    try:
        # Check if the character is a digit or an alphabetic character
        if c.isdigit() or c.isalpha():
            # Check if the character belongs to the Latin script
            return unicodedata.name(c).startswith(('LATIN', 'DIGIT'))
        # Include common European punctuation if needed, adjust as necessary
        common_punctuation = " .,;:-_'\"!?()[]{}"
        if c in common_punctuation:
            return True
        # Return False for any other character
        return False
    except ValueError:
        return False

def filter_european_characters(s):
    return ''.join(c if is_european_character(c) else '_' for c in s)

import string
base_reasonable_chars = set( string.ascii_letters + string.digits + " " + string.punctuation )
def filter_unreasonable_characters(s):
    # c.isalnum() lets umlauts, etc, through
    return ''.join(c if c in base_reasonable_chars or c.isalnum() else '_' for c in s)

def filter_nonprintable_characters(s):
    return ''.join(c if c.isprintable() else '_' for c in s)

def filter_chars_for_irods(s):
    return filter_nonprintable_characters(s)

# This ignores the AVU units field.
def pydict_to_irods_avus( coll_or_obj, metadata_dict ):

    AVU_list = []

    # Load the dict into a list of AVU ops
    for key, value in metadata_dict.items():
        logging.debug( "Create AVU OP list. ipath: " + coll_or_obj.path
                     + " key: " + key + " type(key): " + str(type(key))
                     + " value: " + value + " type(value): " + str(type(value)) )

        if value == "":
            value = " "

        #if not key.isprintable():
        #    k = filter_chars_for_irods(key)
        #    logging.warning( f"Modify metadata attribute to contain only ascii chars. new: {k} old: {key} ipath: {coll_or_obj.path}" )
        #    hexkey = ":".join("{:02x}".format(c) for c in key.encode())
        #    logging.warning( f"unmodified key hex: {hexkey}" )
        #    key = k
        #
        #if not value.isprintable():
        #    v = filter_chars_for_irods(value)
        #    logging.warning( f"Modify metadata value to contain only ascii chars. new: {v} old: {value} ipath: {coll_or_obj.path}" )
        #    hexvalue = ":".join("{:02x}".format(c) for c in value.encode())
        #    logging.warning( f"unmodified value hex: {hexvalue}" )
        #    value = v

        AVU_list.append( AVUOperation( operation='add', avu=iRODSMeta( key, value ) ) )

    # Try to put in the whole list as an atomic op. (We expect this to work.)
    worked = False
    try:
        coll_or_obj.metadata.apply_atomic_operations( *AVU_list )
        worked = True
        logging.debug( f"Succeeded in adding AVU_list" )
    except Exception as ex:
        logging.error( "Applying list of AVUs as atomic op to collection or object failed. (Will try individual AVUs)"
                     + " ex: " + str(ex)
                     + " ipath: " + coll_or_obj.path
                     + " AVU_list >" + str(AVU_list) + "<" )
        raise ex

    return True

    ## This is the clean return.
    #if worked:
    #    return True
    #
    ################## TRY TO CLEAN UP BY SCRUBBING KEY VALUE FOR AVU
    #
    ## Load the dict into a list of AVU ops
    #avu_op_fail_flag = False
    #for key, value in metadata_dict.items():
    #    logging.debug( "Create AVU OP list. ipath: " + coll_or_obj.path + " key: " + key + " value: " + value )
    #
    #    if value == "":
    #        value = " "
    #
    #    k = convert_non_ascii_to_underscore(key)
    #    if k != key:
    #        logging.warning( f"Modify metadata attribute to conain only ascii chars. new: {k} old: {key} ipath: {coll_or_obj.path}" )
    #        hexkey = ":".join("{:02x}".format(c) for c in key.encode())
    #        logging.warning( f"hexkey: {hexkey}" )
    #        key = k
    #
    #    v = convert_non_ascii_to_underscore(value)
    #    if v != value:
    #        logging.warning( f"Modify metadata value to conain only ascii chars. new: {v} old: {value} ipath: {coll_or_obj.path}" )
    #        hexvalue = ":".join("{:02x}".format(c) for c in value.encode())
    #        logging.warning( f"hexvalue: {hexvalue}" )
    #        value = v
    #
    #    try:
    #        AddAvu( coll_or_obj_saved, key, value )
    #    except Exception as ex:
    #        logging.error( "Failed Applying individual AVUs (after full list failed)."
    #                     + " key: " + str(key)
    #                     + " value: " + str(value)
    #                     + " ipath: " + coll_or_obj_saved.path
    #                     + " ex: " + str(ex) )
    #        avu_op_fail_flag = True
    #        last_ex = ex
    #        continue
    #
    #    logging.info( "Succeeded applying individual AVU (after full list failed)."
    #                 + " key: " + str(key)
    #                 + " value: " + str(value)
    #                 + " ipath: " + coll_or_obj.path )
    #
    #if avu_op_fail_flag:
    #    raise last_ex
    #
    #return True

    #    AVU_list.append( AVUOperation( operation='add', avu=iRODSMeta( key, value ) ) )
    #
    ## Try again with individual avus. Probably will not work. But we will get a little more data.
    #avu_op_fail_flag = False
    #last_ex = None
    #for avu_op in AVU_list:
    #    try:
    #        coll_or_obj.metadata.apply_atomic_operations( *avu_op )
    #    except Exception as ex:
    #        logging.error( "Failed Applying individual AVUs (after full list failed)."
    #                     + " avu_op: " + str( avu_op )
    #                     + " ipath: " + coll_or_obj.path
    #                     + " ex: " + str(ex) )
    #        avu_op_fail_flag = True
    #        last_ex = ex
    #        continue
    #
    #    logging.info( "Succeeded applying individual AVU (after full list failed)."
    #                 + " avu_op: " + str( avu_op )
    #                 + " ipath: " + coll_or_obj.path )
    #
    #if avu_op_fail_flag:
    #    raise last_ex
    #
    #return True


import irods
import os
import asyncio

# In line async fuction to read data blocks
async def async_read_block(file_handle, block_size):
    while True:
        # Read a block of data from the file asynchronously in a separate thread
        block = await asyncio.to_thread(file_handle.read, block_size)
        if not block:
            # End of file reached, break out of loop
            break
        # Return the block of data to the event loop as a coroutine
        yield block

async def async_streaming_transfer_file_to_object( args ):

    file_pathname = args[ 0 ]
    irods_obj     = args[ 1 ]
    block_size    = args[ 2 ]

    rc = 0
    transfer_size = 0

    # Initialize the checksum calculator
    hasher256 = hashlib.sha256()
    hasher512 = hashlib.sha512()

    try:
        while True:
            # Read a block of data from the file asynchronously
            block = await read_block(file_pathname, block_size)
            if not block:
                # End of file reached, break out of loop
                break
            transfer_size += len(block)
            # Write the block of data to the temporary object in iRODS
            tmp_obj.write(block)
            # Flush the buffer to ensure the block is written to iRODS immediately
            tmp_obj.flush()
            # Update the checksum with the block of data
            hasher256.update(block)
            hasher512.update(block)
    except Exception as ex:
        logging.error("Failed to transfer data from file to irods. ipath: " + new_obj_ipath + " ex: " + str(ex) )
        #return None
        rc = -1

    return rc, hasher256, hasher512, transfer_size

def streaming_transfer_file_to_object( args ):

    file_pathname = args[ 0 ]
    irods_sesh    = args[ 1 ]
    irods_ipath   = args[ 2 ]
    block_size    = args[ 3 ]
    watchdog_fx   = args[ 4 ]

    rc = 0
    transfer_size = 0

    # Initialize the checksum calculator
    hasher256 = hashlib.sha256()
    hasher512 = hashlib.sha512()


    target_obj = irods_sesh.data_objects.get(irods_ipath)

    try:
        # irods_obj_handle = irods_sesh.data_objects.open( irods_ipath, mode='w', checksum=True)
        irods_obj_handle = target_obj.open('w', checksum=True)
    except Exception as ex:
        logging.error("Failed open irods object for transfer.  file: " + file_pathname + " to irods ipath: " + irods_ipath + " ex: " + str(ex) )
        raise
        #return -1, hasher256, hasher512, 0

    try:
        with open( file_pathname, 'rb' ) as input_file:
            while True:
                # Read a block of data from the file asynchronously
                if watchdog_fx != None:
                    watchdog_fx()
                block = input_file.read( block_size )
                if not block:
                    # End of file reached, break out of loop
                    break
                transfer_size += len(block)
                if watchdog_fx != None:
                    watchdog_fx()
                # Write the block of data to the object in iRODS
                irods_obj_handle.write(block)
                # Flush the buffer to ensure the block is written to iRODS immediately
                if watchdog_fx != None:
                    watchdog_fx()
                irods_obj_handle.flush()
                # Update the checksum with the block of data
                hasher256.update(block)
                hasher512.update(block)
    except Exception as ex:
        logging.error( "Failed to transfer data from file: " + file_pathname
                     + " to irods_ipath: " + irods_ipath
                     + " ex: " + str(ex) )
        #return None
        #rc = -1
        raise

    irods_obj_handle.close()

    hash_digest256 = hasher256.digest()
    hash_digest512 = hasher512.digest()

    b64_digest = base64.b64encode(hash_digest256).decode()
    new_obj_hash_256 = "sha2:{}".format(b64_digest)

    return rc, hash_digest256, hash_digest512, transfer_size

# Returns None or the data_object irods ref.
def confirmed_put( irods_sesh,
                   file_pathname,
                   new_obj_ipath,
                   metadata_dict=None,
                   datatype=None,
                   block_size=(2**28),
                   replace_existing=False,
                   skip_on_zero_len=False,
                   watchdog_fx=None ):
    logging.debug( "file_pathname: "  + file_pathname
                 + " new_obj_ipath: " + new_obj_ipath
                 + " datatype: "      + datatype
                 + " block_size: "    + str( block_size )
                 + " replace_existing " + str( replace_existing)
                 + " skip_on_zero_len " + str( skip_on_zero_len )
                 + " watchdog_fx "      + str( watchdog_fx) )

    if metadata_dict != None:
        logging.debug( f"metadata_dict: {metadata_dict}" )


    # NOTE: this routine uploades the file as a temporary iRODS object.
    # Common sense suggests checking if the objct already exsists, a rare condition first.
    # The trouble is, that requires checksumming the new object before uploading and
    # for very large objects (many GB) this checksum results in pulling the file from
    # a network file system twice due to blowing cache.
    # So, we upload to temp, checkusmming along the way, and then check for a pre-existing irods object.

    #if irods_sesh == None:
    #    logging.error( "irods_sesh == None" )
    #    return None

    # Figure out if this path in the archive is occupied with pre-existing data.
    existing_obj = None
    try:
        options = {kw.VERIFY_CHKSUM_KW: ''}
        existing_obj = irods_sesh.data_objects.get( new_obj_ipath, **options )
        logging.debug( "Object aleady exists at ipath: " + new_obj_ipath )
    except irods_ex.DataObjectDoesNotExist:
        logging.debug( "DataObjectDoesNotExist " + new_obj_ipath )
    except irods_ex.OBJ_PATH_DOES_NOT_EXIST:
        logging.debug( "OBJ_PATH_DOES_NOT_EXIST " + new_obj_ipath )
    except Exception as ex:
        logging.error("Failed using data_onject.get() (but not DataObjectDoesNotExist) ipath: " + new_obj_ipath + " ex: " + str(ex) + " type " + str(type(ex)) )
        raise

    try:
        upload_file_size = os.path.getsize( file_pathname )
    except Exception as ex:
        logging.error( f"FATAL: Failed to get size of file to upload. file_pathname: {file_pathname} ex: {ex}" )
        raise

    # Return if zero sized objects are to be discarded. Check if an existing object needs to be removed.
    if upload_file_size == 0 and skip_on_zero_len:
        if existing_obj != None and replace_existing:
            logging.warning( f"Got zero len file. Removing existing iRODS obj. file_pathname: {file_pathname} ipath: {new_obj_ipath}" )
            try:
                existing_obj.unlink(force=True)
            except Exception as ex:
                logging.error( "Failed remove existing zero line file on replace_existing and skip_on_zero_len. ipath: {new_obj_ipath}  ex: {ex} " )
                raise
        else:
            logging.debug( f"Got zero len file with skip_on_zero_len set. Returning. file_pathname: {file_pathname} ipath: {new_obj_ipath}" )
        return 0

    # Make a timestamped partial tmp file name which, if the upload somehow fails here, will be left behind
    # Hopefully this does not happen much, but we have seen at least on case so far.
    # Cleanup will need to be done elsewhere.
    new_obj_ipath_tmp = new_obj_ipath + ".part." + datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%fZ%z")

    logging.debug("Uploading file: " + file_pathname + " to tmp ipath: " + new_obj_ipath_tmp )

    try:
        tmp_obj = irods_sesh.data_objects.create( new_obj_ipath_tmp )
    except Exception as axe:
        logging.error( "Falied to create temp irods data object."
                     + " file_pathname: " + file_pathname
                     + " tmp ipath: "     + new_obj_ipath_tmp
                     + " ex: " + str(axe) )
        raise

    run_args = [ file_pathname, irods_sesh, new_obj_ipath_tmp, block_size, watchdog_fx ]

    # Cannot use asyncio with < python 3.7. Cannot use goto-statement with > python 3.6
    # run_results = asyncio.run( streaming_transfer_file_to_object( run_args ) )

    run_results = streaming_transfer_file_to_object( run_args )

    transfer_rc    = run_results[ 0 ]
    hash_digest256 = run_results[ 1 ]
    hash_digest512 = run_results[ 2 ]
    transfer_size  = run_results[ 3 ]

    if transfer_rc < 0:   # Maybe not needed ... take out the rc part of the return
        logging.error( "Falied transfer file to irods oobject."
                     + " file_pathname: " + file_pathname
                     + " tmp ipath: "     + new_obj_ipath_tmp )
        sys.exit( -1 )

    # Example here: https://github.com/irods/python-irodsclient/blob/main/irods/test/helpers.py#L251
    # ... in function: def compute_sha256_digest(file_path):
    b64_digest256 = base64.b64encode(hash_digest256).decode()
    new_obj_hash_256 = "sha2:{}".format(b64_digest256)

    b64_digest512 = base64.b64encode(hash_digest512).decode()
    new_obj_hash_512 = "sha2:{}".format(b64_digest512)

    if metadata_dict == None:
        metadata_dict = {}

    # Put the hash and datatype into the dict
    metadata_dict[ "SHA512SUMB64" ] = new_obj_hash_512

    if datatype == None:
        datatype = "UNKNOWN"

    metadata_dict[ "DataType" ] = datatype

    try:
        pydict_to_irods_avus( tmp_obj, metadata_dict )
    except Exception as ex:
        logging.error("Failed to create AVUS on iRODS object. ipath: " + new_obj_ipath + " ex: " + str(ex) )
        logging.error("metadata AVU dict >" + str( metadata_dict ) + "<" )
        raise

    mismatch = False

    if existing_obj != None:

        # https://github.com/irods/python-irodsclient/blob/main/irods/test/collection_test.py#L317
        #file_digest = helpers.compute_sha256_digest( file_pathname )

        #logging.error( "irods checksum: " + str( existing_obj.chksum() ) + " new_obj_hash_256 " + str( new_obj_hash_256 ) + " file_digest " + str( file_digest ) )

        if existing_obj.chksum() != new_obj_hash_256 :
            mismatch = True
            mismatch_reason = "data csm"
            logging.debug( "Object pre-exists but data mismatches."
                         + " file_pathname " + file_pathname )
        else:
            # Get the existing objects metadata as a python dict
            existing_obj_dict = irods_avus_to_pydict( existing_obj )

            if existing_obj_dict != metadata_dict:
                logging.debug( "Object matches on data but mismatches metadata."
                             + " new avu dict: "  + str( metadata_dict )
                             + " existing dict: " + str( existing_obj_dict )
                             + " file_pathname: " + file_pathname )
                mismatch = True
                mismatch_reason = "metadata"

        if mismatch:
            # Mismatch existing object. Move the old object out of the way and date it.
            dup_ipath = existing_obj.path + "_dup_" + datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%fZ%z")
            try:
                irods_sesh.data_objects.move( existing_obj.path, dup_ipath )
            except Exception as ex:
                logging.error( "Failed to move an existing object out of the way so can not upload current version."
                             + " path " + existing_obj.path
                             + " ex: " + str( ex ) )
                raise
            logging.warn( f"Moved mismatching existing object (reason: {mismatch_reason}) to ipath: {dup_ipath}" )
            # Fall through to move newly uploaded object into place.
        else:
            # Matching existing object. Remove the tmp object we just uploaded and leave the old one.
            try:
                tmp_obj.unlink(force=True)
            except Exception as ex:
                logging.error( "Failed to inlink newly created, but duplicate object."
                             + " new_obj_ipath_tmp: " + str( new_obj_ipath_tmp )
                             + " ex: " + str( ex ) )
                raise

            logging.warn( "Existing object matched data and metadata at ipath (removed uploaded tmp object): "
                        + " ipath: " + new_obj_ipath )
            return transfer_size

    # Move the tmp object to the proper object ipath.
    try:
        irods_sesh.data_objects.move( new_obj_ipath_tmp, new_obj_ipath )
    except Exception as ex:
        logging.error( "Failed to move the tmp object to the proper logical path."
                     + " new_obj_ipath_tmp: " + new_obj_ipath_tmp
                     + " new_obj_ipath: " + new_obj_ipath
                     + " ex: " + str( ex ) )
        raise

    logging.debug( "file " + file_pathname + " in place at ipath " + new_obj_ipath )

    if existing_obj != None and replace_existing:
        logging.warning( f"replace_existing flag set. Removing old data object. ipath: {dup_ipath}" )
        try:
            dup_obj = irods_sesh.data_objects.get( dup_ipath )
            dup_obj.unlink(force=True)
        except Exception as ex:
            logging.warning( "Failed remove dup when remove_existing flag set. ipath: {dup_ipath}  ex: {ex} " )
            raise

    return transfer_size

     #new_obj = None
     #try:
     #    new_obj = irods_sesh.data_objects.get( new_obj_ipath )
     #except Exception as ex:
     #    logging.error("Failed data_onject.open(). ipath: " + new_obj_ipath + " ex: " + str(ex) )
     #
     #
     #logging.debug( "Returning new object handle for: "
     #             + " file_pathname: " + file_pathname
     #             + " new_obj.ipath: " + new_obj.path )
     #
     #return new_obj


# Bring in the experiment validation code
sys.path.append(os.path.join(os.path.dirname(__file__), '../mrdata_irods_config'))
import mrdata_irods_config as miconf


# Returns False, error_message or True, dummystr
def SetupExperimentInIRODS( irods_sesh, irods_zone, irods_user, exp_dict, logger=None ):
    ## Create the iRODS representation
    experiment_id = exp_dict[ "ExperimentId" ]
    this_exp_num  = exp_dict[ "ExperimentNumber" ]
    exp_form_study = exp_dict[ "StudyId" ]

    mrdata_metadata_expid_ipath = miconf.get_metadata_experiment_ipath( str(experiment_id) )

    # Create this experiment -- watch out for dupes
    # This method worries me w.r.t race conditions between the get and create
    # ToDo It feels like there should be a better way.
    try:
        irods_experiment_check = irods_sesh.collections.get( mrdata_metadata_expid_ipath )
        message = "SYSTEM ERROR: Please report to admins. Experiment ID in use. experiment id: " \
                  + experiment_id + " exp num: " + this_exp_num
        logger.error( message )
        return False, message
    except Exception as err:
        logger.info( "iRODS experiment will be assigned name: " + experiment_id \
                          + "  fyi: irods.get() err was: " + str( err ) )

    try:
        metadata_expid_coll = irods_sesh.collections.create(mrdata_metadata_expid_ipath, recurse=True)
    except Exception as ex:
        message = "MRData iRODS metadata experiment collection for data could not get'ed or created. experiment ipath: " + mrdata_metadata_expid_ipath
        logger.error( message + " ex: " + str(ex))
        return False, message

    # Kind of a double check
    if mrdata_metadata_expid_ipath != metadata_expid_coll.path:
        message = "FAILED to access/create an iRODS metadata experiment collection. "
        logger.error( message + mrdata_metadata_expid_ipath)
        return False, message

    logger.info("MRData iRODS echodata experiment collection created. " +  \
        " experiment_id: " + experiment_id +                               \
        " ipath: " + mrdata_metadata_expid_ipath )

    pydict_to_irods_avus( metadata_expid_coll, exp_dict )

    return True, "Succeed"


#import pathlib
#pypath = pathlib.Path(__file__).resolve().parent
#modpath = os.path.join( pypath, "../experiment_id" )
#sys.path.append( modpath )
sys.path.append(os.path.join(os.path.dirname(__file__), '../experiment_id'))
import experiment_id as exp_id_fx


# Return True and yaml string or False and an error message
# This interface needs to be, um, refined a bit.
def RegisterNewExperiment( exp_form_owner,
                           exp_form_subject,
                           exp_form_study,
                           exp_form_scanner,
                           exp_form_scan_type,
                           exp_form_description,
                           exp_form_url,
                           cred_file_name,
                           repo_git_cmd,
                           repo_path,
                           repo_lock_path,
                           force_exp_number=None,
                           logger=None,
                           time_out_secs=None,
                           accept_undefined_owner=False
                           ):


    irods_sesh, mrdataforms_base_path = get_irods_session_from_cred_yaml( cred_file_name, time_out_secs=time_out_secs )
    if irods_sesh == None:
        message = "Failed to open iRODS session."
        logger.error( message + " cred_file_name: " + str( cred_file_name ) )
        return None, message

    # This should come from mrdata-common
    irods_user = str( irods_sesh.username )
    irods_zone = str( irods_sesh.zone )
    cast_base_ipath  = "/" + irods_zone + "/home/castellum"

    with irods_sesh: # Ensure irods_sesh is disconnected on any return.

        # It's a real question whether we need to do this level of userid verification - is it just to keep the riffraff out?
        irods_owner_user, error_message = get_irods_user(irods_sesh, exp_form_owner)
        if irods_owner_user == None and not accept_undefined_owner:
            message = "Experiment owner " + exp_form_owner + " is not a registered MrData userid."
            logger.info( message )
            return None, message

        # Castellum Metadata in iRODS: /MRDataZone/home/castellum/studies/10/subjects

        cast_study_id = str( exp_form_study )
        cast_study_ipath = cast_base_ipath + "/studies/" + cast_study_id
        try:
            cast_study_coll = irods_sesh.collections.get( cast_study_ipath )
        except Exception as err:
            error_msg = "Failed to find cast_study_id in irods castellum metadata. cast_study_id " + cast_study_id \
                        + " cast_study_ipath " + cast_study_ipath
            logger.error( error_msg + " err: " + str(err))
            message = "Castellum study " + cast_study_id + " is not registered as active in MrData."
            if not accept_undefined_owner:
                return None, message

        logger.debug( "Got castellum metadata study from iRODS."
                      " cast_study_id: " + cast_study_id + " cast_study_ipath: " + cast_study_ipath )

        cast_subject_psi = str( exp_form_subject )
        cast_subject_ipath = cast_study_ipath + "/subjects/" + cast_subject_psi
        logger.info( "cast_subject_ipath " + cast_subject_ipath )
        try:
            cast_subject_coll = irods_sesh.collections.get( cast_subject_ipath )
        except Exception as err:
            error_msg = "Failed to find cast_subject_psi with irods server. cast_subject_psi " + cast_subject_psi
            logger.error( error_msg + " err: " + str(err))
            message = "Castellum subject does not exist in castellum metadata in iRODS."
            return None, message

        logger.debug( "Got mrdata subject. cast_subject_psi=" + cast_subject_psi \
                    + " mrdata_cast_ipath=" + str( cast_subject_ipath ) )

        repo_lock = FileLock(repo_lock_path, timeout=10)
        logger.info( "repo_lock_path: " + repo_lock_path
                   + " repo_lock: " + str(repo_lock) )

        repo_index_file = repo_path + "/app_index_file.txt"
        logger.info( "repo_index_file " + repo_index_file )

        reg_uuid = uuid.uuid1()
        this_exp_number = -1

        # If the repo lock is not aquired on the first try, wait 120 secs and then try again.
        # This does not imply multi-thread safety. It mainly allows remove a repo lock automatically.
        for hack in range(2):
            try:
                repo_lock.acquire(timeout=10)
                logger.info( "Got git repo lock" )
                break;
            except Timeout:
                if hack == 0: # Somehow, we get escapes from the 'with repo_lock:' below -- this clears old log files
                    lock_age_limit = 120.0
                    logger.error("Repo lock is in place. Waiting " + str(lock_age_limit) + " seconds and then deleting." )
                    exists = os.path.exists(repo_lock_path)
                    timeout =   ((time.time() - os.path.getmtime(repo_lock_path)) > lock_age_limit)
                    logger.error( str(exists), str(timeout) )
                    if exists and timeout:
                        os.remove(repo_lock_path)
                        logger.error("repo_lock_file " + repo_lock_path +
                            " failed to lock and was more than" + str(lock_age_limit) +
                            " seconds old. It has been deleted to try again" )
                else:
                    message = "Failed to get git repo lock. Try again in " + str(lock_age_limit) + " seconds"
                    logger.error( message )
                    return None, message

        # Unfortunitly this does re-aquire the lock which increments a counter
        with repo_lock: # Ensure unlock on any return

            repo_lock.release() # Decrement the lock counter to 1

            git_reset_cmd = repo_git_cmd + "; cd " + repo_path + "; git fetch; git reset --hard origin/master; git clean -fdx; git pull"
            errno = os.system( git_reset_cmd )
            if errno != 0:
                message = "os.system() to reset repo failed. errno: " + str(errno) + " git_reset_cmd: " + git_reset_cmd
                logger.error( message )
                return None, message

            # For testing, create a file with the next experiment id starting at 100k. Consider moving this elsewhere.
            if os.getenv( "MRDENV_MODE" ) == "TEST" :
                if not os.path.isfile( repo_index_file ) :
                    with open( repo_index_file, "x" ) as f: # read/write/create but pointer is at end of file
                        print( str( 100000 ) + " " + str( reg_uuid ), file=f )
                    logger.debug( "Created app file at " + repo_index_file + " for use in TEST mode." )

            with open( repo_index_file, "a+" ) as f: # read/write/create but pointer is at end of file
                f.seek(0)
                words = f.readline().split()
                last_exp_number   = int(words[0])
                last_exp_uuid     = words[1]
                this_exp_number   = last_exp_number + 1

                logger.debug( "Next experiment number based on repo index file is: " + str( this_exp_number) )

                # Figure out if we need to right the forced exp_number to the next value for expidsi
                # TODO: THIS COULD BE BETTER DONE BY UPTAKING THE EXP ID AS A PARAMETER
                if os.getenv( "MRDENV_MODE" ) != "PROD" and force_exp_number != None :
                    logger.info( "Force of experiment number: " + str(force_exp_number) )

                    # Only write the exp number out if it is a new high.
                    if int(force_exp_number) >= int(this_exp_number) :
                        logger.info( "Updating next exp number from " + str( last_exp_number ) + " to " + str( force_exp_number ) )
                        f.truncate(0)
                        print( str( force_exp_number ) + " " + str( reg_uuid ), file=f )
                    else:
                        logger.info( "*** WARN *** Got force exp number that is lower than the next exp number."
                                   + " this_exp_number: "  + str( this_exp_number ) + " force_exp_number: " + str( force_exp_number ) )
                    this_exp_number = force_exp_number
                # Normal production path
                else:
                    f.truncate(0)
                    print( str( this_exp_number ) + " " + str( reg_uuid ), file=f )



            if this_exp_number == -1:
                message = "Failed to update the index file and aquire a new experiment number"
                logger.error( message )
                return None, message

            logger.debug( "this_exp_number: " + str(this_exp_number ) )
            experiment_id = str( exp_id_fx.experiment_number_to_id( this_exp_number ) )
            logger.debug( "experiment_id: " + experiment_id )
            assert exp_id_fx.experiment_id_validate( experiment_id )  # make sure the experiment name validates

            reg_date_time = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%fZ%z")

            dict_file = \
            {    'Status'               : "Valid"
                ,'ExperimentId'         : experiment_id
                ,'ExperimentNumber'     : str( this_exp_number )
                ,'UUID'                 : str( reg_uuid )
                ,'Owner'                : exp_form_owner
                ,'SubjectId'            : exp_form_subject
                ,'StudyId'              : exp_form_study
                ,'Scanner'              : exp_form_scanner
                ,'ScanType'             : exp_form_scan_type
                ,'URL'                  : str( exp_form_url )
                ,'Description'          : str( exp_form_description )
                ,'RegistrationDateTime' : reg_date_time
            }

            #new_experiment_file = repo_path + "/" + str(this_exp_number) + ".yml"
            #new_experiment_file = repo_path + miconf.get_metadata_experiment_ipath( str(experiment_id) )

            #new_experiment_dir  = repo_path + "/" + miconf.get_metadata_experiment_ipath( str(experiment_id) )
            #os.makedirs( new_experiment_dir, exist_ok=True )
            #new_experiment_file = new_experiment_file + "/"  + experiment_id + ".yml"

            new_experiment_dir  = repo_path + "/" + miconf.get_metadata_experiment_ipath( str(experiment_id) )
            os.makedirs( new_experiment_dir, exist_ok=True )
            # new_experiment_file = new_experiment_dir + ".dm.sdori_" + experiment_id + ".yml"
            last_slash_index = new_experiment_dir.rfind("/")
            # print( "last_slash_index: " + str( last_slash_index ) )
            # prefix the yaml file for the new path
            # new_experiment_file = new_experiment_dir[:last_slash_index] + "/" + ".dm.sdori_" + new_experiment_dir[last_slash_index + 1:] + ".yml"
            # Use .o.sdori. for object metadata and .c.sdori. for collection metadata.
            new_experiment_file = new_experiment_dir + "/" + ".o.sdori" + new_experiment_dir[last_slash_index + 1:] + ".yml"


            try:
                with open( new_experiment_file, "w") as file:
                    documents = yaml.dump_all([dict_file], file, explicit_start=True, explicit_end=True)
            except Exception as err:
                message = "Failed to write Experiment YAML file to git to set initial invalid."
                logger.error( message + " experiment number: " + str( this_exp_id ) + " err: " + str(err))
                mradmin_experiment = irods_sesh.objects.remove( mrdata_experiment_path )
                return None, message

            logger.info( "Created/wrote: repo_index_file " + repo_index_file + " new_experiment_file    " + new_experiment_file )

            git_commit_cmd = ( repo_git_cmd +
                               "&& cd " + repo_path +
                               "&& git add " + repo_index_file + " " + new_experiment_file +
                               "&& git commit -m'" + str(experiment_id) +"' " + repo_index_file + " " + new_experiment_file +
                               "&& git push" )

            logger.debug( "git_commit_cmd >" + git_commit_cmd + "<")

            errno = os.system( git_commit_cmd )

            if errno != 0:
                message = "Failed: Commit/Push of " + repo_index_file + " errno=" + str(errno)
                logger.error( message )
                return None, message

            rc_bool, fail_message = SetupExperimentInIRODS( irods_sesh, irods_zone, irods_user, dict_file, logger=logger )

            if not rc_bool :
                logger.error( "Failed in SetupExperimentInIRODS() message: " + fail_message )
                return None, fail_message

            # DOES NOT SEEM TO WORK
            # This is a test since exiting anywhere else would be a sesh leak -- e.g. no cleanup()
            # This is in case with irods_sesh does not really work.
            logger.debug( "Calling cleanup on irods_sesh.")
            irods_sesh.cleanup()
            return experiment_id, new_experiment_file



import os
import sys
import errno
import time
import datetime
import glob
import uuid
import yaml
import re
import logging

from pathlib import Path

from irods.session import iRODSSession
from irods.meta import iRODSMeta, AVUOperation

def AddAvu( coll, avu_attr, avu_value ):
    #try:
    #
    #except Exception as ex:

    try:
        coll.metadata.add( avu_attr, avu_value )
    except Exception as ex:
        logging.error( "Failed to attach AVU attr: " + avu_attr +
                       " avu_value: " + avu_value +
                       " coll: " + coll.path + " ex: " + str(ex) )
        return False
    return True


def init():
    pass


# This scrubs off the AVU units field
def irods_avus_to_pydict( coll_or_obj ):

    # We expect metadata AVUs on the metadata experiment collection
    try:
        all_AVUs = coll_or_obj.metadata.items()
    except Exception as ex:
        logging.error("FAILED to access AVUs on echtdata experiment iRODS collection." +
            " ipath: " + mrdata_experiment_ipath + " ex: " + str(ex) )
        return None

    avu_dict = {}
    for avu in all_AVUs:
        logging.debug("coll_or_obj.path: " + coll_or_obj.path + " AVU: " + str(avu) )
        avu_dict[ avu.name ] = avu.value

    return avu_dict

# This ignores the AVU units field.
def pydict_to_irods_avus( coll_or_obj, metadata_dict ):


    #key   = strip_outer_quotes( key.strip() )
    #value = strip_outer_quotes( value.strip() )

    for key, value in metadata_dict.items():
        logging.debug( "ipath: " + coll_or_obj.path + " key: " + key + " value: " + value )

        if value == "":
            value == " "

        coll_or_obj.metadata.apply_atomic_operations( AVUOperation( operation='add', avu=iRODSMeta( key, value )))

        #                                       AVUOperation(operation='add',    avu=iRODSMeta('a2','v2','those_units')),
        #                                       AVUOperation(operation='remove', avu=iRODSMeta('a3','v3')) # , ...



